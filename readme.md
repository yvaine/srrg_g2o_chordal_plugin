## Chordal Distance PGO: g2o plugin
Pose-Graph optimization is a crucial component of many modern SLAM systems. Most prominent state of the art systems address this problem by iterative non-linear least squares. Both number of iterations and convergence basin of these approaches depend on the error functions used to describe the problem. The smoother and more convex the error function with respect to perturbations of the state variables is, the better the least-squares solver will perform.
We propose an alternative error function obtained by removing some non-linearities from the standard used one - i.e. the geodesic error function. Comparative experiments conducted on common benchmarking datasets confirm that our function is more robust to noise that affects the rotational component of the pose measurements and, thus, exhibits a larger convergence basin than the geodesic. Furthermore, its implementation is relatively easy w.r.t. the geodesic distance. This property leads to rather simple derivatives and nice numerical properties of the Jacobians resulting from the effective computation of the quadratic approximation used by Gauss-Newton algorithm.

### Getting started
This is a minimal invasive plugin for g2o that allows to use the chordal
distance for 3d pose graph optimization. The following instructions will allow to embed
it in your local g2o library and to run some benchmarks.

#### Prerequisites
1. Cholmod and CSparse libraries
```bash
sudo apt install libsuitesparse-dev
```

2. Working g2o library. If you don't have installed, you can download and follow the installation steps from [here](https://github.com/RainerKuemmerle/g2o).

#### Installation
1. Copy the folder `pose3d_chordal_plugin` in your
local folder `<g2o_root>/g2o/examples`

2. Add the following lines to the file `<g2o_root>/g2o/examples/CMakeLists.txt`
```cmake
if(CSPARSE_FOUND AND CHOLMOD_FOUND)
    add_subdirectory(pose3d_chordal_plugin)
endif()
```

3. Clean build g2o:
```bash
cd <g2o_root>/build
make clean
cmake ..
make
```

### How to run
This plugin will add some binaries to your current g2o library. Here you can find their description.

1. `geodesic_noise_adder3d`: allows to add to a standard g2o pose-graph - i.e. that uses _geodesic_ error function - white noise on the translational and the rotational component of the pose. Input graph must be at the optimum. Example of usage:
```bash
./geodesic_noise_adder3d -noiseRotation "0.1;0.1;0.1" -noiseTranslation "0.001;0.001;0.001" -o <output_file.g2o> <input_file.g2o>
```

2. `converter_geodesic2chordal`: converts a standard g2o pose-graph into a chordal g2o pose-graph. We suggest to use the default parameters for the Omega conversion. Example:
```bash
./converter_geodesic2chordal -condType 0 -omegaTresh 0.1 -o <chordal_file.g2o> <input_file.g2o>
```

3. `converter_chordal2geodesic`: converts chordal g2o pose-graph back into its geodesic formulation. Example:
```bash
./converter_chordal2geodesic -o <reprojected_file.g2o> <input_file.g2o>
```

4. `g2o_chordal_geodesic_comparator`: this app performs the optimization on a **chordal graph only**. It requires **also the geodesic formulation** of the input, in order to recompute at each iteration the chi2 using the geodesic distance.
```bash
./g2o_chordal_geodesic_comparator -typeslib ${G2O_ROOT}/lib/libg2o_types_chordal3d.so -i 101 -solver gn_fix6_3_cholmod -o <output_file_chordal> -compareStats <stats_file_chordal> -summary <summary_file_chordal> -geodesicGraph <geodesic_graph.g2o> <input_file_chordal.g2o>
```

5. `g2o_chordal_app`: g2o app with statistic that include the initial guess (iteration -1). It can be used with every kind of graph. To show the info type:
```bash
./g2o_chordal_app -h
```

It is good to notice that whenever you want to run g2o apps with chordal datasets, you must add the flag `-typeslib ${G2O_ROOT}/lib/libg2o_types_chordal3d.so`, in order to load the dynamic library of the plugin.

### How to run comparative benchmarks
If you want to run benchmarks on a set of datasets, in the folder `bench_scripts` there are some scripts to easily set up experiments. In this section it is explained how to run benchmarks using those scripts. We recommend to use global paths in every script to avoid any kind of issue. **Important**: scripts won't work if you haven't set the variable `G2O_ROOT` in your `.bashrc` file.
1. **Benchmarks prerequisites**:
geodesic datasets on which you would like to run benchmarks must be contained in a single folder, that we will indicate here as `DATASET_ROOT`. They must be *at the optimum* and in the standard formulation - i.e. they must contain only `VERTEXSE3` and `EDGESE3:QUAT`.

2. **Add noise**: next step requires to add noise to all the datasets. Running
```bash
./noisify_standard_all.sh <DATASET_ROOT>
```
will add to each dataset 4 noise levels (high - mid - low and rot) imposing spherical covariances. If you want to try non-spherical covariances you can run
```bash
./noisify_standard_1axis.sh <DATASET_ROOT>
```

3. **Generate chordal graphs**: once that we have set up the geodesic datasets, it's possible to convert each dataset to the chordal formulation. To do this, simply run:
```bash
./chordalize.sh <DATASET_ROOT>
```

4. **Run benchmarks**: finally you can run comparative benchmarks. We provide 4 scripts to run different optimization algorithms with or without robust kernels. Each scripts - beside the dataset root folder - requires the path to a folder where g2o will save optimization statistics - e.g. chi2, time, ecc. For example, if you want to run benchmarks using Gauss-Newton with Cauchy robust kernel, you can run:
```bash
./run_experiments_gn_cauchy.sh <DATASET_ROOT> <STATS_FOLDER>
```
Of course you can tweak the scripts to create your personal benchmarks flavor, choosing  optimization algorithm, solver and robust kernel from the ones offered by g2o.

In the folder `dataset` we provide some popular synthetic and real dataset on which run some benchmarks.

### Octave code
In the folder `octave` we provide a simple implementation of a LS optimization algorithm based on the chordal distance. It embeds also other kind of edges.

### Authors
- Irvin Aloise
- Giorgio Grisetti

### Something isn't working?
[Contact the maintainer](mailto:ialoise@diag.uniroma1.it) or open an issue :)

### Related publications
Please cite our most recent article if you use our software:
```
@misc{1809.00952,
    Author = {Irvin Aloise and Giorgio Grisetti},
    Title = {Matrix Difference in Pose-Graph Optimization},
    Year = {2018},
    Eprint = {arXiv:1809.00952},
}
```

### License
BSD-4 clauses license
