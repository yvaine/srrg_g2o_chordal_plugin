#pragma once

#include "g2o/config.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/base_binary_edge.h"
#include "g2o/core/hyper_graph_action.h"

//ia include all the headers related to the types_chordal3d library
#include "vertex_se3_chord.h"
#include "edge_se3_chord.h"
