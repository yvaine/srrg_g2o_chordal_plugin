#include "types_chordal3d.h"
#include "g2o/core/factory.h"
#include "g2o/stuff/macros.h"

#include <iostream>

namespace g2o {
  //ia group
  G2O_REGISTER_TYPE_GROUP(chordal3d);

  //ia types
  G2O_REGISTER_TYPE(VERTEX_SE3:CHORD, VertexSE3Chord);
  G2O_REGISTER_TYPE(EDGE_SE3:CHORD, EdgeSE3Chord);

  //ia actions
  G2O_REGISTER_ACTION(VertexSE3ChordWriteGnuplotAction);
#ifdef G2O_HAVE_OPENGL
  G2O_REGISTER_ACTION(VertexSE3ChordDrawAction);
  G2O_REGISTER_ACTION(EdgeSE3ChordDrawAction);
#endif
  
} //ia end namespace g2o
