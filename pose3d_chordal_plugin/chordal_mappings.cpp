#include "chordal_mappings.h"

namespace g2o {
  namespace internal {

    Vector12 toFlatten(const Isometry3& t) {
      Vector12 v = Vector12::Zero();
      v.block<3,1>(0,0) = t.matrix().block<3,1>(0,0);
      v.block<3,1>(3,0) = t.matrix().block<3,1>(0,1);
      v.block<3,1>(6,0) = t.matrix().block<3,1>(0,2);
      v.block<3,1>(9,0) = t.matrix().block<3,1>(0,3);
      return v;
    }
    
      
    Isometry3 fromFlatten(const Vector12& v,
                          const bool reconditionate_rotation) {
      Isometry3 t = Isometry3::Identity();
      t.matrix().block<3,1>(0,0) = v.block<3,1>(0,0);
      t.matrix().block<3,1>(0,1) = v.block<3,1>(3,0);
      t.matrix().block<3,1>(0,2) = v.block<3,1>(6,0);
      t.matrix().block<3,1>(0,3) = v.block<3,1>(9,0);

      if (reconditionate_rotation) {
        const Matrix3& R = t.linear();
        Eigen::JacobiSVD<Matrix3> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Matrix3 R_enforced = svd.matrixU() * svd.matrixV().transpose();
        t.linear() = R_enforced;
      }

      return t;
    }
    
  } //ia end namespace internal
} //ia end namespace g2o
