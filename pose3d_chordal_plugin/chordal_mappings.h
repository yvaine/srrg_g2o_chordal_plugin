#pragma once

#include "g2o/config.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include <Eigen/Core>
#include <Eigen/Geometry>

namespace g2o {

  //! @brief eigen types
  using Vector9  = VectorN<9>;
  using Vector12 = VectorN<12>;
  using Matrix6  = Eigen::Matrix<number_t,6,6,Eigen::ColMajor>;
  using Matrix12 = Eigen::Matrix<number_t,12,12,Eigen::ColMajor>;

  namespace internal {

    //! @brief converts an Isometry3 into a 12 vector [rx ry rz t]
    G2O_TYPES_SLAM3D_API Vector12 toFlatten(const Isometry3& t);

    //! @brief converts a 12 vector [rx ry rz t] into an Isometry3;
    //! @param reconditionate_rotation: if true reconditionate the
    //!        rotation matrix R.
    G2O_TYPES_SLAM3D_API Isometry3 fromFlatten(const Vector12& v, const bool reconditionate_rotation);    
  } //ia end namespace internal
  
} //ia end namespace g2o
